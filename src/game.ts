// a scene by @miliv_eth for the Pangea x Plover Animation Bounty

// import {scene} from "./scene"
import * as utils from "@dcl/ecs-scene-utils"

//enable these for live deploy later
import { createDispenser } from './booth/dispenser'

//poap dispenser
createDispenser(
  {
    position: new Vector3(8, 0, 8),
    rotation: Quaternion.Euler(0, 45, 0),
    scale: new Vector3(1,1,1)
  },
  'acd27e4b-24bd-4040-b715-c0e11e863fb0' //"dcl_event_uuid, replace with custom eevent"
)

// //button
// const button = new Entity("button")
// engine.addEntity(button)
//
// const buttonGLTFShape = new GLTFShape("models/poap/button.glb")
//   buttonGLTFShape.withCollisions = true
//   buttonGLTFShape.isPointerBlocker = true
//   buttonGLTFShape.visible = true
// button.addComponentOrReplace(buttonGLTFShape)
//
// const buttontransform = new Transform({
//   position: new Vector3(8, 0, 8),
//   rotation: new Quaternion(0, 0, 0, 1),
//   scale: new Vector3(1, 1, 1)
// })
// button.addComponentOrReplace(buttontransform)

//feathers
const feathers = new Entity("feathers")
engine.addEntity(feathers)

const feathersGLTFShape = new GLTFShape("models/feathers.glb")
  feathersGLTFShape.withCollisions = true
  feathersGLTFShape.isPointerBlocker = true
  feathersGLTFShape.visible = true
feathers.addComponentOrReplace(feathersGLTFShape)

const featherstransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
feathers.addComponentOrReplace(featherstransform)

//feathers_collider
const feathers_collider = new Entity("feathers_collider")
engine.addEntity(feathers_collider)

const feathers_colliderGLTFShape = new GLTFShape("models/feathers_collider.glb")
  feathers_colliderGLTFShape.withCollisions = true
  feathers_colliderGLTFShape.isPointerBlocker = true
  feathers_colliderGLTFShape.visible = true
feathers_collider.addComponentOrReplace(feathers_colliderGLTFShape)

const feathers_collidertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
feathers_collider.addComponentOrReplace(feathers_collidertransform)

//insta
const insta = new Entity("insta")
insta.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://www.instagram.com/ploveranimation/")
  })
)
engine.addEntity(insta)

const instaGLTFShape = new GLTFShape("models/insta.glb")
  instaGLTFShape.withCollisions = true
  instaGLTFShape.isPointerBlocker = true
  instaGLTFShape.visible = true
insta.addComponentOrReplace(instaGLTFShape)

const instatransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
insta.addComponentOrReplace(instatransform)

//insta_cover
const insta_cover = new Entity("insta_cover")
insta_cover.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://www.instagram.com/ploveranimation/")
  })
)
engine.addEntity(insta_cover)

const insta_coverGLTFShape = new GLTFShape("models/insta_cover.glb")
  insta_coverGLTFShape.withCollisions = true
  insta_coverGLTFShape.isPointerBlocker = true
  insta_coverGLTFShape.visible = true
insta_cover.addComponentOrReplace(insta_coverGLTFShape)

const insta_covertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
insta_cover.addComponentOrReplace(insta_covertransform)

//insta_collider
const insta_collider = new Entity("insta_collider")
engine.addEntity(insta_collider)

const insta_colliderGLTFShape = new GLTFShape("models/insta_collider.glb")
  insta_colliderGLTFShape.withCollisions = true
  insta_colliderGLTFShape.isPointerBlocker = true
  insta_colliderGLTFShape.visible = true
insta_collider.addComponentOrReplace(insta_colliderGLTFShape)

const insta_collidertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
insta_collider.addComponentOrReplace(insta_collidertransform)

//fb
const fb = new Entity("fb")
fb.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://www.facebook.com/ploveranimation/")
  })
)
engine.addEntity(fb)

const fbGLTFShape = new GLTFShape("models/fb.glb")
  fbGLTFShape.withCollisions = true
  fbGLTFShape.isPointerBlocker = true
  fbGLTFShape.visible = true
fb.addComponentOrReplace(fbGLTFShape)

const fbtransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fb.addComponentOrReplace(fbtransform)

//fb_cover
const fb_cover = new Entity("fb_cover")
fb_cover.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://www.facebook.com/ploveranimation/")
  })
)
engine.addEntity(fb_cover)

const fb_coverGLTFShape = new GLTFShape("models/fb_cover.glb")
  fb_coverGLTFShape.withCollisions = true
  fb_coverGLTFShape.isPointerBlocker = true
  fb_coverGLTFShape.visible = true
fb_cover.addComponentOrReplace(fb_coverGLTFShape)

const fb_covertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fb_cover.addComponentOrReplace(fb_covertransform)

//fb_collider
const fb_collider = new Entity("fb_collider")
fb_collider.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://www.facebook.com/ploveranimation/")
  })
)
engine.addEntity(fb_collider)

const fb_colliderGLTFShape = new GLTFShape("models/fb_collider.glb")
  fb_colliderGLTFShape.withCollisions = true
  fb_colliderGLTFShape.isPointerBlocker = true
  fb_colliderGLTFShape.visible = true
fb_collider.addComponentOrReplace(fb_colliderGLTFShape)

const fb_collidertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fb_collider.addComponentOrReplace(fb_collidertransform)

//discord
const discord = new Entity("discord")
discord.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://discord.com/invite/hearzDspGh")
  })
)
engine.addEntity(discord)

const discordGLTFShape = new GLTFShape("models/discord.glb")
  discordGLTFShape.withCollisions = true
  discordGLTFShape.isPointerBlocker = true
  discordGLTFShape.visible = true
discord.addComponentOrReplace(discordGLTFShape)

const discordtransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
discord.addComponentOrReplace(discordtransform)

//discord_cover
const discord_cover = new Entity("discord_cover")
discord_cover.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://discord.com/invite/hearzDspGh")
  })
)
engine.addEntity(discord_cover)

const discord_coverGLTFShape = new GLTFShape("models/discord_cover.glb")
  discord_coverGLTFShape.withCollisions = true
  discord_coverGLTFShape.isPointerBlocker = true
  discord_coverGLTFShape.visible = true
discord_cover.addComponentOrReplace(discord_coverGLTFShape)

const discord_covertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
discord_cover.addComponentOrReplace(discord_covertransform)

//discord_collider
const discord_collider = new Entity("discord_collider")
discord_collider.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://discord.com/invite/hearzDspGh")
  })
)
engine.addEntity(discord_collider)

const discord_colliderGLTFShape = new GLTFShape("models/discord_collider.glb")
  discord_colliderGLTFShape.withCollisions = true
  discord_colliderGLTFShape.isPointerBlocker = true
  discord_colliderGLTFShape.visible = true
discord_collider.addComponentOrReplace(discord_colliderGLTFShape)

const discord_collidertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
discord_collider.addComponentOrReplace(discord_collidertransform)

//linkedin
const linkedin = new Entity("linkedin")
linkedin.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://www.linkedin.com/company/plover-animation/")
  })
)
engine.addEntity(linkedin)

const linkedinGLTFShape = new GLTFShape("models/linkedin.glb")
  linkedinGLTFShape.withCollisions = true
  linkedinGLTFShape.isPointerBlocker = true
  linkedinGLTFShape.visible = true
linkedin.addComponentOrReplace(linkedinGLTFShape)

const linkedintransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
linkedin.addComponentOrReplace(linkedintransform)

//linkedin_cover
const linkedin_cover = new Entity("linkedin_cover")
linkedin_cover.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://www.linkedin.com/company/plover-animation/")
  })
)
engine.addEntity(linkedin_cover)

const linkedin_coverGLTFShape = new GLTFShape("models/linkedin_cover.glb")
  linkedin_coverGLTFShape.withCollisions = true
  linkedin_coverGLTFShape.isPointerBlocker = true
  linkedin_coverGLTFShape.visible = true
linkedin_cover.addComponentOrReplace(linkedin_coverGLTFShape)

const linkedin_covertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
linkedin_cover.addComponentOrReplace(linkedin_covertransform)

//linkedin_collider
const linkedin_collider = new Entity("linkedin_collider")
linkedin_collider.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://www.linkedin.com/company/plover-animation/")
  })
)
engine.addEntity(linkedin_collider)

const linkedin_colliderGLTFShape = new GLTFShape("models/linkedin_collider.glb")
  linkedin_colliderGLTFShape.withCollisions = true
  linkedin_colliderGLTFShape.isPointerBlocker = true
  linkedin_colliderGLTFShape.visible = true
linkedin_collider.addComponentOrReplace(linkedin_colliderGLTFShape)

const linkedin_collidertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
linkedin_collider.addComponentOrReplace(linkedin_collidertransform)

//plover
const plover = new Entity("plover")
engine.addEntity(plover)

const ploverGLTFShape = new GLTFShape("models/plover.glb")
  ploverGLTFShape.withCollisions = true
  ploverGLTFShape.isPointerBlocker = true
  ploverGLTFShape.visible = true
plover.addComponentOrReplace(ploverGLTFShape)

const plovertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
plover.addComponentOrReplace(plovertransform)

//plover_collider
const plover_collider = new Entity("plover_collider")
plover_collider.addComponent(
  new OnPointerDown(() => {
    openExternalURL("http://ploveranimation.com/")
  })
)
engine.addEntity(plover_collider)

const plover_colliderGLTFShape = new GLTFShape("models/plover_collider.glb")
  plover_colliderGLTFShape.withCollisions = true
  plover_colliderGLTFShape.isPointerBlocker = true
  plover_colliderGLTFShape.visible = true
plover_collider.addComponentOrReplace(plover_colliderGLTFShape)

const plover_collidertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
plover_collider.addComponentOrReplace(plover_collidertransform)

//twitter
const twitter = new Entity("twitter")
twitter.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://twitter.com/ploveranimation")
  })
)
engine.addEntity(twitter)

const twitterGLTFShape = new GLTFShape("models/twitter.glb")
  twitterGLTFShape.withCollisions = true
  twitterGLTFShape.isPointerBlocker = true
  twitterGLTFShape.visible = true
twitter.addComponentOrReplace(twitterGLTFShape)

const twittertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
twitter.addComponentOrReplace(twittertransform)

//twitter_cover
const twitter_cover = new Entity("twitter_cover")
twitter_cover.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://twitter.com/ploveranimation")
  })
)
engine.addEntity(twitter_cover)

const twitter_coverGLTFShape = new GLTFShape("models/twitter_cover.glb")
  twitter_coverGLTFShape.withCollisions = true
  twitter_coverGLTFShape.isPointerBlocker = true
  twitter_coverGLTFShape.visible = true
twitter_cover.addComponentOrReplace(twitter_coverGLTFShape)

const twitter_covertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
twitter_cover.addComponentOrReplace(twitter_covertransform)

//twitter_collider
const twitter_collider = new Entity("twitter_collider")
twitter_collider.addComponent(
  new OnPointerDown(() => {
    openExternalURL("https://twitter.com/ploveranimation")
  })
)
engine.addEntity(twitter_collider)

const twitter_colliderGLTFShape = new GLTFShape("models/twitter_collider.glb")
  twitter_colliderGLTFShape.withCollisions = true
  twitter_colliderGLTFShape.isPointerBlocker = true
  twitter_colliderGLTFShape.visible = true
twitter_collider.addComponentOrReplace(twitter_colliderGLTFShape)

const twitter_collidertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
twitter_collider.addComponentOrReplace(twitter_collidertransform)

// //egg
// const egg = new Entity("egg")
// engine.addEntity(egg)
//
// const eggtransform = new Transform({
//   position: new Vector3(8, 0, 8),
//   rotation: new Quaternion(0, 0, 0, 1),
//   scale: new Vector3(1.1, 1.1, 1.1)
// })
// egg.addComponentOrReplace(eggtransform)
//
// const eggGLTFShape = new GLTFShape("models/egg.glb")
//   eggGLTFShape.withCollisions = true
//   eggGLTFShape.isPointerBlocker = true
//   eggGLTFShape.visible = true
// egg.addComponentOrReplace(eggGLTFShape)

//birdie
const birdie = new Entity("birdie")
engine.addEntity(birdie)

const birdietransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1.1, 1.1, 1.1)
})
birdie.addComponentOrReplace(birdietransform)

const birdieGLTFShape = new GLTFShape("models/birdie.glb")
  birdieGLTFShape.withCollisions = true
  birdieGLTFShape.isPointerBlocker = true
  birdieGLTFShape.visible = true
birdie.addComponentOrReplace(birdieGLTFShape)

//debris
const debris = new Entity("debris")
engine.addEntity(debris)

const debristransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1.1, 1.1, 1.1)
})
debris.addComponentOrReplace(debristransform)

const debrisGLTFShape = new GLTFShape("models/debris.glb")
  debrisGLTFShape.withCollisions = true
  debrisGLTFShape.isPointerBlocker = true
  debrisGLTFShape.visible = true
debris.addComponentOrReplace(debrisGLTFShape)

//debris_collider
const debris_collider = new Entity("debris_collider")
engine.addEntity(debris_collider)

const debris_collidertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1.1, 1.1, 1.1)
})
debris_collider.addComponentOrReplace(debris_collidertransform)

const debris_colliderGLTFShape = new GLTFShape("models/debris_collider.glb")
  debris_colliderGLTFShape.withCollisions = true
  debris_colliderGLTFShape.isPointerBlocker = true
  debris_colliderGLTFShape.visible = true
debris_collider.addComponentOrReplace(debris_colliderGLTFShape)

//tv1
const myVideoClip1 = new VideoClip(
  'https://player.vimeo.com/external/552481870.m3u8?s=c312c8533f97e808fccc92b0510b085c8122a875'
)
const myVideoTexture1 = new VideoTexture(myVideoClip1)

const myMaterial1 = new Material()
myMaterial1.albedoTexture = myVideoTexture1
myMaterial1.roughness = 1
myMaterial1.specularIntensity = 0
myMaterial1.metallic = 0

const screen1 = new Entity()
screen1.addComponent(new PlaneShape())

const screen1transform = new Transform({
  position: new Vector3(3.5, 1, 12.25),
  rotation: new Quaternion(-10, 160, 20, 98),
//  rotation: new Quaternion(20, 0, 15, -3),
  scale: new Vector3(7, 6, 6)
})
screen1.addComponentOrReplace(screen1transform)

screen1.addComponent(myMaterial1)
screen1.addComponent(
  new OnPointerDown(() => {
    myVideoTexture1.playing = !myVideoTexture1.playing
  })
)
engine.addEntity(screen1)

// #enabling autoplay>>heavier gpu usage
//myVideoTexture1.play()

//tv2
const myVideoClip2 = new VideoClip(
  'https://player.vimeo.com/external/552481870.m3u8?s=c312c8533f97e808fccc92b0510b085c8122a875'
)
const myVideoTexture2 = new VideoTexture(myVideoClip2)

const myMaterial2 = new Material()
myMaterial2.albedoTexture = myVideoTexture2
myMaterial2.roughness = 1
myMaterial2.specularIntensity = 0
myMaterial2.metallic = 0

const screen2 = new Entity()
screen2.addComponent(new PlaneShape())

const screen2transform = new Transform({
  position: new Vector3(12.25, 1, 3.5),
  rotation: new Quaternion(30, 70, 10, -233),
//rotation: new Quaternion(-10, 160, 20, 98),
//rotation: new Quaternion(10, 160, -20, 53),
  scale: new Vector3(7, 6, 6)
})
screen2.addComponentOrReplace(screen2transform)

screen2.addComponent(myMaterial2)
screen2.addComponent(
  new OnPointerDown(() => {
    myVideoTexture2.playing = !myVideoTexture2.playing
  })
)
engine.addEntity(screen2)

// #enabling autoplay>>heavier gpu usage
//myVideoTexture2.play()

//plane
const plane = new Entity("plane")
engine.addEntity(plane)

const planetransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1.1, 1.1, 1.1)
})
plane.addComponentOrReplace(planetransform)

const planeGLTFShape = new GLTFShape("models/plane.glb")
  planeGLTFShape.withCollisions = true
  planeGLTFShape.isPointerBlocker = true
  planeGLTFShape.visible = true
plane.addComponentOrReplace(planeGLTFShape)

//towerdecimate
const towerdecimate = new Entity("towerdecimate")
engine.addEntity(towerdecimate)

const towerdecimatetransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1.1, 1.1, 1.1)
})
towerdecimate.addComponentOrReplace(towerdecimatetransform)

const towerdecimateGLTFShape = new GLTFShape("models/towerdecimate.glb")
  towerdecimateGLTFShape.withCollisions = true
  towerdecimateGLTFShape.isPointerBlocker = true
  towerdecimateGLTFShape.visible = true
towerdecimate.addComponentOrReplace(towerdecimateGLTFShape)

//towerdecimate
const towerdecimate_collider = new Entity("towerdecimate_collider")
engine.addEntity(towerdecimate_collider)

const towerdecimate_collidertransform = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1.1, 1.1, 1.1)
})
towerdecimate_collider.addComponentOrReplace(towerdecimate_collidertransform)

const towerdecimate_colliderGLTFShape = new GLTFShape("models/towerdecimate_collider.glb")
  towerdecimate_colliderGLTFShape.withCollisions = true
  towerdecimate_colliderGLTFShape.isPointerBlocker = true
  towerdecimate_colliderGLTFShape.visible = true
towerdecimate_collider.addComponentOrReplace(towerdecimate_colliderGLTFShape)

//setup scene
export let scene = {
  feathers:{
    entity: feathers,
    transform: featherstransform,
    GLTFShape: feathersGLTFShape,
  },
  feathers_collider:{
    entity: feathers_collider,
    transform: feathers_collidertransform,
    GLTFShape: feathers_colliderGLTFShape,
  },
  insta:{
    entity: insta,
    transform: instatransform,
    GLTFShape: instaGLTFShape,
  },
  insta_cover:{
    entity: insta_cover,
    transform: insta_covertransform,
    GLTFShape: insta_coverGLTFShape,
  },
  insta_collider:{
    entity: insta_collider,
    transform: insta_collidertransform,
    GLTFShape: insta_colliderGLTFShape,
  },
  fb:{
    entity: fb,
    transform: fbtransform,
    GLTFShape: fbGLTFShape,
  },
  fb_cover:{
    entity: fb_cover,
    transform: fb_covertransform,
    GLTFShape: fb_coverGLTFShape,
  },
  fb_collider:{
    entity: fb_collider,
    transform: fb_collidertransform,
    GLTFShape: fb_colliderGLTFShape,
  },
  discord:{
    entity: discord,
    transform: discordtransform,
    GLTFShape: discordGLTFShape,
  },
  discord_cover:{
    entity: discord_cover,
    transform: discord_covertransform,
    GLTFShape: discord_coverGLTFShape,
  },
  discord_collider:{
    entity: discord_collider,
    transform: discord_collidertransform,
    GLTFShape: discord_colliderGLTFShape,
  },
  linkedin:{
    entity: linkedin,
    transform: linkedintransform,
    GLTFShape: linkedinGLTFShape,
  },
  linkedin_cover:{
    entity: linkedin_cover,
    transform: linkedin_covertransform,
    GLTFShape: linkedin_coverGLTFShape,
  },
  linkedin_collider:{
    entity: linkedin_collider,
    transform: linkedin_collidertransform,
    GLTFShape: linkedin_colliderGLTFShape,
  },
  plover:{
    entity: plover,
    transform: plovertransform,
    GLTFShape: ploverGLTFShape,
  },
  plover_collider:{
    entity: plover_collider,
    transform: plover_collidertransform,
    GLTFShape: plover_colliderGLTFShape,
  },
  twitter:{
    entity: twitter,
    transform: twittertransform,
    GLTFShape: twitterGLTFShape,
  },
  twitter_cover:{
    entity: twitter_cover,
    transform: twitter_covertransform,
    GLTFShape: twitter_coverGLTFShape,
  },
  twitter_collider:{
    entity: twitter_collider,
    transform: twitter_collidertransform,
    GLTFShape: twitter_colliderGLTFShape,
  },
  birdie:{
    entity: birdie,
    transform: birdietransform,
    GLTFShape: birdieGLTFShape,
    },
  debris:{
    entity: debris,
    transform: debristransform,
    GLTFShape: debrisGLTFShape,
      },
  debris_collider:{
    entity: debris_collider,
    transform: debris_collidertransform,
    GLTFShape: debris_colliderGLTFShape,
      },
  button:{
    entity: button,
    transform: buttontransform,
    GLTFShape: buttonGLTFShape,
      },
  egg:{
    entity: egg,
    transform: eggtransform,
    GLTFShape: eggGLTFShape,
    },
  // egg_collider:{
  //   entity: egg_collider,
  //   transform: egg_collidertransform,
  //   GLTFShape: egg_colliderGLTFShape,
  //   },
  plane:{
    entity: plane,
    transform: planetransform,
    GLTFShape: planeGLTFShape,
    },
  towerdecimate:{
    entity: towerdecimate,
    transform: towerdecimatetransform,
    GLTFShape: towerdecimateGLTFShape,
  },
  towerdecimate_collider:{
    entity: towerdecimate_collider,
    transform: towerdecimate_collidertransform,
    GLTFShape: towerdecimate_colliderGLTFShape,
    }
}




// --- Set up a system / original code by DCL---
//
// class RotatorSystem {
//   // this group will contain every entity that has a Transform component
//   group = engine.getComponentGroup(Transform)
//
//   update(dt: number) {
//     // iterate over the entities of the group
//     for (let entity of this.group.entities) {
//       // get the Transform component of the entity
//       const transform = entity.getComponent(Transform)
//
//       // mutate the rotation
//       transform.rotate(Vector3.Up(), dt * 10)
//     }
//   }
// }
//
// // Add a new instance of the system to the engine
// engine.addSystem(new RotatorSystem())
//
// /// --- Spawner function ---
//
// function spawnCube(x: number, y: number, z: number) {
//   // create the entity
//   const cube = new Entity()
//
//   // add a transform to the entity
//   cube.addComponent(new Transform({ position: new Vector3(x, y, z) }))
//
//   // add a shape to the entity
//   cube.addComponent(new BoxShape())
//
//   // add the entity to the engine
//   engine.addEntity(cube)
//
//   return cube
// }
//
// /// --- Spawn a cube ---
//
// const cube = spawnCube(8, 1, 8)
//
// cube.addComponent(
//   new OnClick(() => {
//     cube.getComponent(Transform).scale.z *= 1.1
//     cube.getComponent(Transform).scale.x *= 0.9
//
//     spawnCube(Math.random() * 8 + 1, Math.random() * 8, Math.random() * 8 + 1)
//   })
// )
//
// let avocado = new Entity()
// avocado.addComponent(new GLTFShape("models/avocado.gltf"))
// avocado.addComponent(new Transform({
//     position: new Vector3(3, 1, 3),
//     scale: new Vector3(10, 10, 10)
//     }))
//     avocado.addComponent(new utils.ScaleTransformComponent(
//          new Vector3(1,1,1),
//          new Vector3(5, 5, 5),
//          10
//      ))
// engine.addEntity(avocado)
