// import * as utils from "@dcl/ecs-scene-utils"
//
// //core
// const core = new Entity("core")
// engine.addEntity(core)
//
// const coretransform = new Transform({
//   position: new Vector3(8, 0, 8),
//   rotation: new Quaternion(0, 0, 0, 1),
//   scale: new Vector3(.87, .87, .87)
// })
// core.addComponentOrReplace(coretransform)
//
// const coreGLTFShape = new GLTFShape("models/craze/core.glb")
//   coreGLTFShape.withCollisions = true
//   coreGLTFShape.isPointerBlocker = true
//   coreGLTFShape.visible = true
// core.addComponentOrReplace(coreGLTFShape)
//
// // //feathers
// // const feathers = new Entity("feathers")
// // engine.addEntity(feathers)
// //
// // const featherstransform = new Transform({
// //   position: new Vector3(8, 0, 8),
// //   rotation: new Quaternion(0, 0, 0, 1),
// //   scale: new Vector3(.87, .87, .87)
// // })
// // feathers.addComponentOrReplace(featherstransform)
// //
// // const feathersGLTFShape = new GLTFShape("models/feathers.glb")
// //   feathersGLTFShape.withCollisions = true
// //   feathersGLTFShape.isPointerBlocker = true
// //   feathersGLTFShape.visible = true
// // feathers.addComponentOrReplace(feathersGLTFShape)
//
// //feather1
// const feather1 = new Entity("feather1")
// engine.addEntity(feather1)
//
// const feather1GLTFShape = new GLTFShape("models/feather1.glb")
//   feather1GLTFShape.withCollisions = true
//   feather1GLTFShape.isPointerBlocker = true
//   feather1GLTFShape.visible = true
// feather1.addComponentOrReplace(feather1GLTFShape)
//
// const feather1transform = new Transform({
//   position: new Vector3(8, 0, 8),
//   rotation: new Quaternion(0, 0, 0, 1),
//   scale: new Vector3(1, 1, 1)
// })
//
// feather1.addComponentOrReplace(feather1transform)
//
// //feather2
// const feather2 = new Entity("feather2")
// engine.addEntity(feather2)
//
// const feather2GLTFShape = new GLTFShape("models/feather2.glb")
//   feather2GLTFShape.withCollisions = true
//   feather2GLTFShape.isPointerBlocker = true
//   feather2GLTFShape.visible = true
// feather2.addComponentOrReplace(feather2GLTFShape)
//
// const feather2transform = new Transform({
//   position: new Vector3(8, 0, 8),
//   rotation: new Quaternion(0, 0, 0, 1),
//   scale: new Vector3(1, 1, 1)
// })
//
// feather2.addComponentOrReplace(feather2transform)
//
// //feather3
// const feather3 = new Entity("feather3")
// engine.addEntity(feather3)
//
// const feather3GLTFShape = new GLTFShape("models/feather3.glb")
//   feather3GLTFShape.withCollisions = true
//   feather3GLTFShape.isPointerBlocker = true
//   feather3GLTFShape.visible = true
// feather3.addComponentOrReplace(feather3GLTFShape)
//
// const feather3transform = new Transform({
//   position: new Vector3(8, 0, 8),
//   rotation: new Quaternion(0, 0, 0, 1),
//   scale: new Vector3(1, 1, 1)
// })
//
// feather3.addComponentOrReplace(feather3transform)
//
// //plane
// const plane = new Entity("plane")
// engine.addEntity(plane)
//
// const planetransform = new Transform({
//   position: new Vector3(8, 0, 8),
//   rotation: new Quaternion(0, 0, 0, 1),
//   scale: new Vector3(1.1, 1.1, 1.1)
// })
// plane.addComponentOrReplace(planetransform)
//
// const planeGLTFShape = new GLTFShape("models/plane.glb")
//   planeGLTFShape.withCollisions = true
//   planeGLTFShape.isPointerBlocker = true
//   planeGLTFShape.visible = true
// plane.addComponentOrReplace(planeGLTFShape)
//
// //cells
// const cells = new Entity("cells")
// engine.addEntity(cells)
//
// const cellstransform = new Transform({
//   position: new Vector3(8, 0, 8),
//   rotation: new Quaternion(0, 0, 0, 1),
//   scale: new Vector3(1.1, 1.1, 1.1)
// })
// cells.addComponentOrReplace(cellstransform)
//
// const cellsGLTFShape = new GLTFShape("models/cells.glb")
//   cellsGLTFShape.withCollisions = true
//   cellsGLTFShape.isPointerBlocker = true
//   cellsGLTFShape.visible = true
// cells.addComponentOrReplace(cellsGLTFShape)
//
// //setup scene
// export let scene = {
//   core:{
//     entity: core,
//     transform: coretransform,
//     GLTFShape: coreGLTFShape,
//   },
//   // feathers:{
//   //   entity: feathers,
//   //   transform: featherstransform,
//   //   GLTFShape: feathersGLTFShape,
//   // },
//   feather1:{
//     entity: feather1,
//     transform: feather1transform,
//     GLTFShape: feather1GLTFShape,
//   },
//   feather2:{
//     entity: feather2,
//     transform: feather2transform,
//     GLTFShape: feather2GLTFShape,
//   },
//   feather3:{
//     entity: feather3,
//     transform: feather3transform,
//     GLTFShape: feather3GLTFShape,
//   },
//   plane:{
//     entity: plane,
//     transform: planetransform,
//     GLTFShape: planeGLTFShape,
//   },
//   cells:{
//     entity: cells,
//     transform: cellstransform,
//     GLTFShape: cellsGLTFShape,
//   }
// }
